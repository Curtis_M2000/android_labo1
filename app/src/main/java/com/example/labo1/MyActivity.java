package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class MyActivity extends AppCompatActivity {
    private static final String myPref = "Students";
    private int mode = Activity.MODE_PRIVATE;
    private String id;

    private ArrayList<String> listActivity =  new ArrayList<String>();
    private ArrayAdapter<String> listAdapter;
    private ListView list1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        SharedPreferences sharedPreferences = getSharedPreferences(myPref, mode);
        id = sharedPreferences.getString("idStudent", "No Name");

        list1 = (ListView)findViewById(R.id.listMyActivities);
        final MyActivity.bgWorker bgworker = new MyActivity.bgWorker(this);
        bgworker.execute(0, id);

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog alert = new AlertDialog.Builder(MyActivity.this).create();
                alert.setTitle("Cancel Participation");
                alert.setMessage("Are you sure you want cancel your participation?");

                alert.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }
                );

                final String nameAct = ((TextView)view).getText().toString();

                alert.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyActivity.bgWorker bgworker2 = new MyActivity.bgWorker(MyActivity.this);
                        SharedPreferences sharedPreferences = getSharedPreferences(myPref, mode);
                        String id1 = sharedPreferences.getString("idStudent", "No Name");
                        bgworker2.execute(1, id1, nameAct);

                        listActivity.remove(position);
                        listAdapter.notifyDataSetChanged();
                    }
                });
                alert.show();
            }
        });
    }

    public class bgWorker extends AsyncTask{
        public Context ctx;
        String cible = "";
        URL url = null;
        HttpURLConnection con;
        String finalMsg = "";
        boolean redirect = false;
        AlertDialog alertB;
        int pos;

        public bgWorker(Context ctx){ this.ctx = ctx; }

        @Override
        protected void onPreExecute(){
            alertB = new AlertDialog.Builder(this.ctx).create();
        }

        @Override
        protected String doInBackground(Object[] param){
            switch ((int)param[0]){
                case 0 :
                    cible = "https://androidcurtis.000webhostapp.com/myActivity.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("id", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            listActivity.add(line);
                        }
                        initArrayAdapter();

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                break;

                case 1 :
                    cible = "https://androidcurtis.000webhostapp.com/unsubscribe.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("id", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8") + "&"
                                + URLEncoder.encode("name", "utf-8") + "="
                                + URLEncoder.encode((String)param[2], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            finalMsg = line;
                        }
                        redirect = true;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                break;
            }
            return finalMsg;
        }

        @Override
        protected void onPostExecute(Object o){
            if((String)o != ""){
                alertB.setMessage((String)o);
                alertB.show();
            }
            list1.setAdapter(listAdapter);
        }
    }

    private void initArrayAdapter()
    {
        listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listActivity);
    }
}
