package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class SelectedActivity extends AppCompatActivity {
    private static final String myPref = "Students";
    private int mode = Activity.MODE_PRIVATE;
    private String myActivity;

    private ArrayList<String> listActivity =  new ArrayList<String>();
    private ArrayAdapter<String> listAdapter;
    private ListView list1;

    private double averageGrade = 0;
    private int nb = -2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected);

        TextView selectedActivity = (TextView)findViewById(R.id.selectedActivity);
        TextView average = (TextView)findViewById(R.id.avgGrade);
        TextView nbSubscribed = (TextView)findViewById(R.id.nbSubscribed);

        SharedPreferences sharedPreferences = getSharedPreferences(myPref, mode);
        myActivity = sharedPreferences.getString("activityChosen", "No Name");

        selectedActivity.setText(myActivity);
        list1 = (ListView)findViewById(R.id.listParticipatingStudents);

        SelectedActivity.bgWorker bgworker = new SelectedActivity.bgWorker(this);
        bgworker.execute(0, myActivity);

        while(average.getText().equals("")) {
            if(averageGrade != 0){
                average.setText("  Average Grade = " + (averageGrade == -1.0 ? "n/a" : String.valueOf(averageGrade)));
                nbSubscribed.setText("  Participating Students (" + String.valueOf(nb) + ") : ");

                initArrayAdapter();
                list1.setAdapter(listAdapter);
            }
        }
    }

    public class bgWorker extends AsyncTask{
        public Context ctx;
        String cible = "";
        URL url = null;
        HttpURLConnection con;

        public bgWorker(Context ctx){ this.ctx = ctx; }

        @Override
        protected void onPreExecute(){ }

        protected String doInBackground(Object[] param){
            switch ((int)param[0]){
                case 0:
                    cible = "https://androidcurtis.000webhostapp.com/loadStudentsInActivity.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("name", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            listActivity.add(line);
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    cible = "https://androidcurtis.000webhostapp.com/participatingStudents.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("name", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            nb = Integer.parseInt(line);
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    cible = "https://androidcurtis.000webhostapp.com/averageGrade.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("name", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            if(checkDouble(line)){
                                averageGrade = Double.parseDouble(line);
                            }
                            else{
                                averageGrade = -1;
                            }
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                break;
            }

            return null;
        }

        protected void onPostExecute(Object o){ }
    }

    private void initArrayAdapter()
    {
        listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listActivity);
    }

    private boolean checkDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        }
        catch(NumberFormatException e) {
            return false;
        }

    }
}
