package com.example.labo1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class ProfileActivity extends AppCompatActivity {
    private static final String myPref = "Students";
    private int mode = Activity.MODE_PRIVATE;
    private String userName, userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        SharedPreferences sharedPreferences = getSharedPreferences(myPref, mode);
        userId = sharedPreferences.getString("idStudent", "No id");
    }

    public void check(View v){
        TextView name = (TextView)findViewById(R.id.user);
        TextView pwd = (TextView)findViewById(R.id.pwd);

        String userN = name.getText().toString();
        String userP = pwd.getText().toString();

        dbWorker back = new dbWorker(this);
        back.execute(userN, userP, userId);
    }

    public class dbWorker extends AsyncTask {
        public Context ctx;
        AlertDialog alert;

        public dbWorker(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            alert = new AlertDialog.Builder(this.ctx).create();
            this.alert.setTitle("Profile");
        }

        @Override
        protected String doInBackground(Object[] param){
            String cible = "https://androidcurtis.000webhostapp.com/updateprofile.php";
            String finalMsg = "";

            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();

                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                String msg = URLEncoder.encode("user", "utf-8") + "="
                        + URLEncoder.encode((String)param[0], "utf-8") +
                        "&" + URLEncoder.encode("pw", "utf-8") + "="
                        + URLEncoder.encode((String)param[1], "utf-8")+
                        "&" + URLEncoder.encode("id", "utf-8") + "="
                        + URLEncoder.encode((String)param[2], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    finalMsg = line;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return finalMsg;
        }

        @Override
        protected void onPostExecute(Object o){
            this.alert.setMessage((String)o);
            this.alert.show();
        }
    }
}
