package com.example.labo1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class SignupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
    }

    public void check(View v){
        TextView user = (TextView)findViewById(R.id.user);
        TextView pwd = (TextView)findViewById(R.id.pwd);
        TextView email = (TextView)findViewById(R.id.email);
        TextView name = (TextView)findViewById(R.id.name);
        TextView surname = (TextView)findViewById(R.id.surname);

        String userN = name.getText().toString();
        String userS = surname.getText().toString();
        String userE = email.getText().toString();
        String userL = user.getText().toString();
        String userP = pwd.getText().toString();

        SignupActivity.dbWorker back = new SignupActivity.dbWorker(this);
        back.execute(userN, userS, userE, userL, userP);
    }

    public class dbWorker extends AsyncTask {
        public Context ctx;
        AlertDialog alert;

        public dbWorker(Context ctx){
            this.ctx = ctx;
        }

        @Override
        protected String doInBackground(Object[] param){
            String cible = "https://androidcurtis.000webhostapp.com/signup.php";
            String finalMsg = "";

            try {
                URL url = new URL(cible);
                HttpURLConnection con = (HttpURLConnection)url.openConnection();

                con.setDoInput(true);
                con.setDoOutput(true);
                con.setRequestMethod("POST");

                OutputStream outs = con.getOutputStream();
                BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                String msg = URLEncoder.encode("userN", "utf-8") + "="
                        + URLEncoder.encode((String)param[0], "utf-8") + "&" +
                        URLEncoder.encode("userS", "utf-8") + "="
                        + URLEncoder.encode((String)param[1], "utf-8") + "&" +
                        URLEncoder.encode("userE", "utf-8") + "="
                        + URLEncoder.encode((String)param[2], "utf-8") + "&" +
                        URLEncoder.encode("userL", "utf-8") + "="
                        + URLEncoder.encode((String)param[3], "utf-8") + "&" +
                        URLEncoder.encode("userP", "utf-8") + "="
                        + URLEncoder.encode((String)param[4], "utf-8");

                bufw.write(msg);
                bufw.flush();
                bufw.close();
                outs.close();

                InputStream ins = con.getInputStream();
                BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                String line;
                StringBuffer sbuff = new StringBuffer();

                while((line = bufr.readLine()) != null){
                    sbuff.append(line + "\n");

                    finalMsg = sbuff.toString();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return finalMsg;
        }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            alert = new AlertDialog.Builder(this.ctx).create();
            this.alert.setTitle("Login Status");
        }

        @Override
        protected void onPostExecute(Object o){
            this.alert.setMessage((String)o);
            this.alert.show();
        }
    }
}
