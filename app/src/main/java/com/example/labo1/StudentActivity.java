package com.example.labo1;

import androidx.appcompat.app.AppCompatActivity;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class StudentActivity extends AppCompatActivity {
    private static final String myPref = "Students";
    private int mode = Activity.MODE_PRIVATE;
    private String userName, userId;

    private ArrayList<String> listActivity =  new ArrayList<String>();
    private ArrayAdapter<String> listAdapter;
    private ListView list1;

    PopupMenu pop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student);

        SharedPreferences sharedPreferences = getSharedPreferences(myPref, mode);
        userName = sharedPreferences.getString("nameStudent", "No Name");
        userId = sharedPreferences.getString("idStudent", "No id");
        String welcome = "Welcome " + userName;

        TextView tt = (TextView)findViewById(R.id.rtxt);
        tt.setText(welcome);

        list1 = (ListView)findViewById(R.id.list1);
        bgWorker bgworker = new bgWorker(this);
        bgworker.execute(0);

        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PopUpMenu(view);
            }
        });
    }

    private void initArrayAdapter() { listAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, listActivity); }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        MenuItem mi1 = menu.add(Menu.NONE, Menu.FIRST, 0, "PROFILE");
        MenuItem mi2 = menu.add(Menu.NONE, Menu.FIRST + 1, 1, "SUGGEST ACTIVITY");
        MenuItem mi3 = menu.add(Menu.NONE, Menu.FIRST + 2, 3, "SIGN OUT");
        MenuItem mi4 = menu.add(Menu.NONE, Menu.FIRST + 3, 2, "MY ACTIVITIES");
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        super.onOptionsItemSelected(item);
        Intent i;
        final bgWorker bgworker = new bgWorker(StudentActivity.this);

        switch (item.getItemId()){
            case Menu.FIRST:
                i = new Intent(this, ProfileActivity.class);
                startActivity(i);
            break;

            case Menu.FIRST + 1:
                final Dialog dialPop = new Dialog(StudentActivity.this);
                dialPop.setContentView(R.layout.suggest_activity);

                Window window = dialPop.getWindow();
                window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                dialPop.show();
                Button bsuggest = (Button)dialPop.findViewById(R.id.btnSuggest);
                final TextView tsuggest = (TextView)dialPop.findViewById(R.id.editSuggest);

                bsuggest.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(tsuggest.getText().toString().equals("")){
                            Toast tt = Toast.makeText(StudentActivity.this, "Kinda empty", Toast.LENGTH_SHORT);
                            tt.show();
                        }

                        else{
                            bgworker.execute(3, tsuggest.getText().toString());
                            dialPop.cancel();
                        }
                    }
                });

            break;

            case Menu.FIRST + 2:
                i = new Intent(this, MainActivity.class);
                startActivity(i);
            break;

            case Menu.FIRST + 3:
                i = new Intent(this, MyActivity.class);
                startActivity(i);
            break;
        }
        return true;
    }

    public class bgWorker extends AsyncTask{
        public Context ctx;
        AlertDialog alertB;
        boolean redirect = false;

        public bgWorker(Context ctx){ this.ctx = ctx; }

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            alertB = new AlertDialog.Builder(this.ctx).create();
        }

        @Override
        protected String doInBackground(Object[] param){
            String cible = "";
            String finalMsg = "";
            URL url = null;
            HttpURLConnection con;

            switch ((int)param[0]){
                case 0:
                    cible = "https://androidcurtis.000webhostapp.com/loadactivities.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            listActivity.add(line);
                        }
                        initArrayAdapter();
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case 1:
                    cible = "https://androidcurtis.000webhostapp.com/participate.php";
                    try {
                        this.alertB.setTitle("PARTICIPATION");

                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("id", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8") + "&"
                                + URLEncoder.encode("activity", "utf-8") + "="
                                + URLEncoder.encode((String)param[2], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");
                            finalMsg = sbuff.toString();
                        }
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                break;

                case 2:
                    cible = "https://androidcurtis.000webhostapp.com/grade.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("id", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8") + "&"
                                + URLEncoder.encode("activity", "utf-8") + "="
                                + URLEncoder.encode((String)param[2], "utf-8") + "&"
                                + URLEncoder.encode("grade", "utf-8") + "="
                                + URLEncoder.encode((String)param[3].toString(), "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");
                            finalMsg = sbuff.toString();
                        }

                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

                case 3:
                    cible = "https://androidcurtis.000webhostapp.com/suggest.php";

                    try {
                        url = new URL(cible);
                        con = (HttpURLConnection)url.openConnection();

                        con.setDoInput(true);
                        con.setDoOutput(true);
                        con.setRequestMethod("POST");

                        OutputStream outs = con.getOutputStream();
                        BufferedWriter bufw = new BufferedWriter(new OutputStreamWriter(outs, "utf-8"));

                        String msg = URLEncoder.encode("suggest", "utf-8") + "="
                                + URLEncoder.encode((String)param[1], "utf-8");

                        bufw.write(msg);
                        bufw.flush();
                        bufw.close();
                        outs.close();

                        InputStream ins = con.getInputStream();
                        BufferedReader bufr = new BufferedReader(new InputStreamReader(ins, "iso-8859-1"));

                        String line;
                        StringBuffer sbuff = new StringBuffer();

                        while((line = bufr.readLine()) != null){
                            sbuff.append(line + "\n");
                            finalMsg = sbuff.toString();
                        }
                        this.redirect = true;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (ProtocolException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
            return finalMsg;
        }

        @Override
        protected void onPostExecute(Object o){
            if((String)o != ""){
                alertB.setMessage((String)o);
                alertB.show();
            }

            else{
                if(redirect){
                    final Intent i1 = new Intent(StudentActivity.this, StudentActivity.class);
                    final ProgressDialog progress = new ProgressDialog(ctx);
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setMessage("Adding suggested activity...");
                    progress.setIndeterminate(false);

                    final Thread t = new Thread(){
                        @Override
                        public void run(){
                            int jumptime = 0;
                            while(jumptime < 100){
                                try{
                                    jumptime += 1;
                                    progress.setProgress(jumptime);
                                    sleep(40);
                                }
                                catch(InterruptedException e){
                                    e.printStackTrace();
                                }
                            }

                            if(jumptime == 100){
                                progress.cancel();
                                startActivity(i1);
                            }
                        }
                    };
                    t.start();
                    progress.show();
                }
            }
            list1.setAdapter(listAdapter);
        }
    }

    public void PopUpMenu(View v){
        pop = new PopupMenu(this, v, Gravity.RIGHT);
        MenuInflater minf = getMenuInflater();
        minf.inflate(R.menu.popupmenu, pop.getMenu());
        final String chosen = ((TextView)v).getText().toString();
        pop.show();

        pop.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                String title = item.getTitle().toString();
                final bgWorker bgworker = new bgWorker(StudentActivity.this);

                switch(title){
                    case "PARTICIPATE":
                        bgworker.execute(1, userId, chosen);
                    break;

                    case "GRADE":
                        final Dialog dialPop = new Dialog(StudentActivity.this);
                        dialPop.setContentView(R.layout.grade);

                        Window window = dialPop.getWindow();
                        window.setFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND, WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
                        window.setLayout(WindowManager.LayoutParams.FILL_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
                        dialPop.show();
                        Button bgrade = (Button)dialPop.findViewById(R.id.btnGrade);
                        final TextView tgrade = (TextView)dialPop.findViewById(R.id.editgrade);

                        bgrade.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(tgrade.getText().toString().equals("")){
                                    Toast tt = Toast.makeText(StudentActivity.this, "Kinda empty", Toast.LENGTH_SHORT);
                                    tt.show();
                                }

                                else if(Integer.parseInt(tgrade.getText().toString()) > 100 || Integer.parseInt(tgrade.getText().toString()) < 0){
                                    Toast tt = Toast.makeText(StudentActivity.this, "Enter a grade between 0 and 100", Toast.LENGTH_SHORT);
                                    tt.show();
                                }

                                else{
                                    bgworker.execute(2, userId, chosen, tgrade.getText().toString());
                                    dialPop.cancel();
                                }
                            }
                        });

                    break;

                    case "VIEW ACTIVITY":
                        SharedPreferences sharedPreferences = getSharedPreferences(myPref, mode);
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("activityChosen", chosen);
                        edit.commit();

                        Intent i = new Intent(StudentActivity.this, SelectedActivity.class);
                        startActivity(i);
                    break;
                }
                return true;
            }
        });
    }
}
